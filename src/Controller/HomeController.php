<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        $rep = $this->getDoctrine()->getRepository(Users::class);
        $users = $rep->findAll();

        $name = $users[0]->getUsersProducts();

//        dd($name[0]->getProducts()->getTitle());

        return $this->render('home/index.html.twig', [
            'users' => $users,

        ]);
    }
}


