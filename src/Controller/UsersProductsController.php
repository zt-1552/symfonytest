<?php

namespace App\Controller;

use App\Entity\UsersProducts;
use App\Form\UsersProductsType;
use App\Repository\UsersProductsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/users_products")
 */
class UsersProductsController extends AbstractController
{
    /**
     * @Route("/", name="app_users_products_index", methods={"GET"})
     */
    public function index(UsersProductsRepository $usersProductsRepository): Response
    {
        return $this->render('users_products/index.html.twig', [
            'users_products' => $usersProductsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_users_products_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UsersProductsRepository $usersProductsRepository): Response
    {
        $usersProduct = new UsersProducts();
        $form = $this->createForm(UsersProductsType::class, $usersProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usersProductsRepository->add($usersProduct);
            return $this->redirectToRoute('app_users_products_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('users_products/new.html.twig', [
            'users_product' => $usersProduct,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_users_products_show", methods={"GET"})
     */
    public function show(UsersProducts $usersProduct): Response
    {
        return $this->render('users_products/show.html.twig', [
            'users_product' => $usersProduct,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_users_products_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, UsersProducts $usersProduct, UsersProductsRepository $usersProductsRepository): Response
    {
        $form = $this->createForm(UsersProductsType::class, $usersProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usersProductsRepository->add($usersProduct);
            return $this->redirectToRoute('app_users_products_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('users_products/edit.html.twig', [
            'users_product' => $usersProduct,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_users_products_delete", methods={"POST"})
     */
    public function delete(Request $request, UsersProducts $usersProduct, UsersProductsRepository $usersProductsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usersProduct->getId(), $request->request->get('_token'))) {
            $usersProductsRepository->remove($usersProduct);
        }

        return $this->redirectToRoute('app_users_products_index', [], Response::HTTP_SEE_OTHER);
    }
}
