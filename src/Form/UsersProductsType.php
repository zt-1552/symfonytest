<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Users;
use App\Entity\UsersProducts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersProductsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'label' => 'Пользователь',
                'required' => true,
                'class' => Users::class,
                'choice_label' => 'firstName'
            ])
            ->add('products', EntityType::class, [
                'label' => 'Продукт',
                'required' => true,
                'class' => Product::class,
                'choice_label' => 'title'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UsersProducts::class,
        ]);
    }
}
