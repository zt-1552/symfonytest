<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 */
class Users
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\OneToMany(targetEntity=UsersProducts::class, mappedBy="user", fetch="EAGER")
     */
    private $usersProducts;

    public function __construct()
    {
        $this->usersProducts = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return Collection<int, UsersProducts>
     */
    public function getUsersProducts(): Collection
    {
        return $this->usersProducts;
    }

    public function addUsersProduct(UsersProducts $usersProduct): self
    {
        if (!$this->usersProducts->contains($usersProduct)) {
            $this->usersProducts[] = $usersProduct;
            $usersProduct->setUser($this);
        }

        return $this;
    }

    public function removeUsersProduct(UsersProducts $usersProduct): self
    {
        if ($this->usersProducts->removeElement($usersProduct)) {
            // set the owning side to null (unless already changed)
            if ($usersProduct->getUser() === $this) {
                $usersProduct->setUser(null);
            }
        }

        return $this;
    }

}
